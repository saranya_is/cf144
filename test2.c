#include <stdio.h>
float find_area(float r)
{
    float area;
    area=3.141*r*r;
    return area;
}

int main()
{
    float r,area;
    printf("Enter radius of a circle :\n");
    scanf("%f",&r);
    area=find_area(r);
    printf("Area of a circle is %f",area);
    return 0;
}