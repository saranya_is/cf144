#include <stdio.h>
int main()
{
    int a,b;
    printf("Enter two integers to swap :\n");
    scanf("%d%d",&a,&b);
    swap(&a,&b);
    printf("After swapping %d %d\n",a,b);
    return 0;
}

void swap(int *x,int *y)
{
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
}