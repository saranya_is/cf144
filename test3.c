#include <stdio.h>
#include <math.h>
int main()
{
	int a,b,c;
	float D,r1,r2;
	printf("Enter the co-efficients of quadratic equations :\n");
	scanf("%d%d%d",&a,&b,&c);
	D=(b*b)-(4*a*c);
	if(a==0)
	{
		printf("Given quadratic equation is invalid\n");
	}
	
    else if(D>0)
    {
		printf("Roots are real and distinct\n");
		r1=(-b+sqrt(D))/(2*a);
		r2=(-b-sqrt(D))/(2*a);
		printf("r1=%f \t r2=%f",r1,r2);
	}
	else if(D==0)
	{
		printf("Roots are real and equal\n");
		r1=-b/(2*a);
		r2=-b/(2*a);
		printf("r1=%f \t r2=%f",r1,r2);
	}
	else
	{
		printf("Roots are imaginary and distinct\n");
		printf("r1=%d+i%f\n",(-b)/(2*a),(sqrt(-D))/(2*a));
		printf("r2=%d-i%f",(-b)/(2*a),(sqrt(-D))/(2*a));
	}
	return 0;
}
		
